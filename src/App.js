import React from "react";
import ReactDOM from "react-dom";

import './App.css';
import { savePDF } from '@progress/kendo-react-pdf';
//import html2pdf from "html2pdf.js";
//import $ from "jquery";
//import eventBus from "EventBus";
//Date syling
class NameForm extends React.Component {
  constructor(props) {
    super(props);
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    console.log(typeof (date));
    this.state = { selectedDate: date, venue: '', vmname: '', vmtitle: '', vname: '', vtitle: '', generatorPage: true, sign1U: false, sign2U: false, generatePDF: true };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleImage = this.handleImage.bind(this)
  }
  handleChange(event) {
    this.setState(
      {
        [event.target.name]: event.target.value
      }

    );
  }

  handleSubmit() {
    //  alert('A name was submitted: ' + this.state.venue);
    //event.preventDefault();
    // document.getElementsByClassName("wp1").style.display = "block";
    this.setState({ generatePDF: false });
    var element = document.getElementById("printable");
    savePDF(element, {
      paperSize: 'A4', fileName: 'VenueMonkContract.pdf',
      margin: { top: 10, left: 30, right: 30 }
    });

    //html2pdf(element)
    //console.log(element);    
    //getElementsByClassName('wp1');
    // var opt = {
    //   margin: 1,
    //   filename:     'contract.pdf',
    //   html2canvas:  { scale: 1 },
    //   jsPDF:        { unit: 'px', format: 'a4', orientation: 'portrait' }
    // };

    // New Promise-based usage:
    // html2pdf().set(opt).from(element).save();
    // html2pdf(element);
    this.setState({ generatePDF: true });
    // var html = $(id).html();
    // var win = window.print('');  
    // var script = document.createElement('script');
    // script.src = 'js/print.js';
    // win.document.head.appendChild(script);  
    //    eventBus.on('prit', webViewerPrint);
  }
  handlePreview() {
    this.setState({
      generatorPage: false,
      previewPage: true

    })
  }




  handleImage(e) {
    const reader = new FileReader();
    const signState = [e.target.id]
    reader.onload = (e) => {
      console.log(e.target.result)
      this.setState({
        [signState]: [e.target.result],
        generate: false
      });
      this.setState({
        [signState + 'U']: true
      });

    }
    reader.readAsDataURL(e.target.files[0])
  }
  render() {
    return (
      <div>
        {
          this.state.generatorPage ?
            <div className="wp2 container">
              <h2>Agreement Generator</h2>
              
                <span>Venue Name</span>
                <input type="text" value={this.state.venue} name="venue" onChange={this.handleChange} />
              
              <span>Date of contract</span>
              <input type="date" value={this.state.selectedDate} name="selectedDate" id="contractdate" onChange={this.handleChange} />
              <div>
                <b>
                  <h4><label>For Behalf of VenueMonk</label></h4>
                </b>
                Name
            <input type="text" name="vmname" value={this.state.vmname} onChange={this.handleChange} />
                Title
            <input type="text" name="vmtitle" value={this.state.vmtitle} onChange={this.handleChange} />
                Signature:
              {
                  this.state.sign1U ?<div> <img src={this.state.sign1} height="80" width="200" /></div> : null
                }
                <div>
                <input type="file" accept="image/*" onChange={this.handleImage} id="sign1" />
              </div>
              </div>
              <div>
                <h4><label>For Behalf of {this.state.venue}</label></h4>
                <span>Name</span>
                <b><input type="text" name="vname" value={this.state.vname} onChange={this.handleChange} /></b>
                Title
                <b><input type="text" name="vtitle" value={this.state.vtitle} onChange={this.handleChange} /></b>
                Signature:
                {this.state.sign2U ?<div><br /> <img src={this.state.sign2} height="80" width="200" /></div> : null}
                <input type="file" accept="image/*" onChange={this.handleImage} id="sign2" />
              </div>
              <div className="submit" > 
                <input type="submit" value="Generate Preview" onClick={() => this.handlePreview()} />
              </div>
            </div>

            : null
        }
        {
          this.state.previewPage ?
            <div className="wp1" >
              <div id="printable">
                <h3 align="center ">Agreement between VenueMonk.com and {this.state.venue}</h3>
                <h4>We are delighted to have partnered with {this.state.venue} for all event enquiries that <b>Venue Monk</b> shall generate for {this.state.venue} in future as per the below points:</h4>
                <ol>
                  <li>Venue Monk shall share all qualifying leads for {this.state.venue} through an Email/Whatsapp/SMS to the hotel.</li>
                  <li>{this.state.venue} shall also do its best to give utmost efforts to help close leads generated by</li>
                  <li>VenueMonk.</li>
                  <li>This agreement is effective from Date {this.state.selectedDate.toString()}, and shall remain effective for (01) year from the date of signing</li>
                  <li>Commission rate of per person will be paid by {this.state.venue} to Venue Monk for all confirmed business as per the Grid below:</li>
                </ol>
                <table className="styles" align="center">
                  <tbody>
                    <tr>
                      <th>VENUE</th>
                      <th>RATE GRID</th>
                      <th>APPLICABLE COMMISSION</th>
                    </tr>
                    <tr>
                      <td>{this.state.venue}</td>
                      <td>As per Closure Rate</td>
                      <td>10% + GST of Total Revenue</td>
                    </tr>
                  </tbody>
                </table>
                <h4><u>Payment Terms</u></h4>
                <p><b>Payment needs to be done by the Merchant to VenueMonk within 7 working days from the date of the event</b> by either through cheque issued in favour of “PURPLEPATCH ONLINE SERVICES PRIVATE LIMITED” or by making online account transfer to the following account:</p>
                <b>Account Name:</b> PurplePatch Online Services Private Limited
          <br />
                <b>Account Number:</b> 103105001509
          <br />
                <b>IFSC Code:</b> ICIC0001031
          <br />
                <b>Bank: </b> ICICI
          <br />
                <br />
                <b>Important Note:</b> After 7 working days, a late payment fine of 1% for every 15 days of delay would be levied.
          <br />
                This agreement may be extended automatically unless otherwise informed by either party.
          <br />
                <br />
                <div>
                  <div className="one">
                    <u>For Behalf of Venue Monk</u>
                    <br />
                    <b>Name: {this.state.vmname}</b>
                    <br />
                    <b>Title: {this.state.vmtitle}</b>
                    <br />
                    <b>Signature:</b>
                    <br />
                    <img src={this.state.sign1} height="80" width="200" />
                    <br />
                    <b>Date: </b><p className="da">{this.state.selectedDate.toString()}</p>
                    <br />
                    <br />
                  </div>
                  <div className="two">
                    <u>For Behalf of {this.state.venue}</u>
                    <br />
                    <b>Name: {this.state.vname}</b>
                    <br />
                    <b>Title: {this.state.vtitle}</b>
                    <br />
                    <b>Signature:</b>
                    <br />
                    <img src={this.state.sign2} height="80" width="200" />
                    <br />
                    <b>Date: </b><p className="da">{this.state.selectedDate.toString()}</p>
                  </div>
                </div>
              </div>
              {

                this.state.generatePDF ?
                  <div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />

                    <input type="submit" value="Generate PDF" onClick={() => this.handleSubmit()} />
                  </div>
                  : null
              }
            </div>

            : null
        }
      </div>
    );

  }
}

ReactDOM.render(
  <NameForm />,
  document.getElementById('root')
);

//const App = () => {
//  return React.createElement("div", { id: "something-important" }, [
//);
//};

//ReactDom.render(React.createElement(App), document.getElementById("root"));
//jquery
//$("#button").click(newWindow);

//function newWindow(`id`) {

//}
